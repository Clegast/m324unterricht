# Beispielstruktur
max_num = int(input("Bitte geben Sie die maximale Zahl ein: "))
# Primzahlen finden

def isPrime(num):   
    if num > 1:
        for i in range(2,num):
            if (num % i) == 0:
                return None
            else:
                return num
    else:
        return None

primes = []  

for i in range(max_num):
    if isPrime(i) != None:
        primes.append(isPrime(i))

print primes